package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Ad;
import com.delta.springnotes.repos.AdRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller

public class AdController {

    //Spring boot's dependency injection/passing


    private final AdRepository adRepository;

    public AdController(AdRepository adRepository) {
        this.adRepository = adRepository;
    }
    //get all of the ads and display them on ads/index view
    @GetMapping("/ads")
    public String showAds(Model model){


        //findAll is accessing adRepository
        List<Ad> adList = adRepository.findAll();
        model.addAttribute("noAdsFound", adList.size() == 0);
        model.addAttribute("ads", adList);

        return  "ads/index"; //name of the view
    }


    //show the create/add an form
    @GetMapping("/ads/create")
    public String showForm(Model model){
        model.addAttribute("newAd", new Ad());

        //show the create the create view
        return "ads/create";
    }

    @PostMapping("/ads/create")
    public String createAd(@ModelAttribute Ad adToCreate){
        // save the adToCreate parameter
        adRepository.save(adToCreate);

        //redirect the user to a specific url/ to the list of ads -> /ads
        return "redirect:/ads/";


    }

    //controller method to display an individual item
    @GetMapping("/ads/{id}")
    public String showAd(@PathVariable long id, Model model){
        Ad ad = adRepository.getOne(id); // getting an object of Ad based on the id

        model.addAttribute("showAd", ad);

        return "ads/show";
    }

    //controller method that will
    @GetMapping("/ads/{id}/edit")//lets us see view
    public String showEdit(@PathVariable long id, Model model){
        //find the ad object with
        Ad adToEdit = adRepository.getOne(id);
        //give object an attribute name
        model.addAttribute("editAd",adToEdit);

        return "ads/edit";

    }

    @PostMapping("/ads/{id}/edit")
    public String updateAd(@PathVariable long id,
                           @RequestParam(name = "title") String title,
                           @RequestParam(name = "description") String description)

    {
        //find the ad with the passed in id
        Ad foundAd = adRepository.getAdById(id);//NOTE:

        //UPDATE THE AD'S TITTLE AND DESCRIPTION
        foundAd.setTitle(title);
        foundAd.setDescription(description);

        // save the new ad's data changes
        adRepository.save(foundAd);
        //redirect the user to url that contains the list of ads
        return "redirect:/ads/";

    }
    //controller method that will allow the user to delete objects of our class
    @PostMapping("/ads/{id}/delete")
    public String delete(@PathVariable long id){
        //access the deleteByID()
        adRepository.deleteById(id);

        //redirect the user to the url containing the
        //list of ads
        return "redirect:/ads/";
    }

}
