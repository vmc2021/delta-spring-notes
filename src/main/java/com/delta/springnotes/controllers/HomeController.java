package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller



public class HomeController {

    @GetMapping("/")
    @ResponseBody
    public String home(){
        return "Home Page from Spring Boot Lesson";
    }

    //path variables, they are part of our URL
    @GetMapping("/hello/{name}")// name variable
    @ResponseBody
    public String sayHello(@PathVariable String name){
        return "Hello there, " + name + "!";
    }
    //path variable, not a string
    //RequestMapping longer variation of GetMapping
    @RequestMapping(path = "/increment/{number}", method = RequestMethod.GET)
    @ResponseBody
    //path variable is int number
    public String addingOne(@PathVariable int number){
        return number + " plus one is " + (number + 1) + "!";
    }

}//end of class
