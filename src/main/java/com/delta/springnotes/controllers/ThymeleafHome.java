package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller//annotation
public class ThymeleafHome {

    @GetMapping("/thymeleaf-home")//method

    public String welcome(){
        return "thymeleaf-view";//return name of our view "thymeleaf-view"
    }


}
