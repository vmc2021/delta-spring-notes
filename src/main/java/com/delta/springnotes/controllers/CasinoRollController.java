package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller


    public class CasinoRollController {

    public String showCasinoRoll() {
        return  "guess-roll";
    }

    @GetMapping("/casino-roll/{num}")
    public String userGuessedNumber(@PathVariable int num, Model model) {


        int correctNumber = (int) (Math.random()* (5) + 1);

        String message;

        if (correctNumber == num){
            message = "You guessed correctly";
        }
        else {
            message = "You guessed wrong";
        }

        model.addAttribute("showcaseMessage", message);

        return "quess-roll";
    }




}
