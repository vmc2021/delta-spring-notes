package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller

public class DataController {
    //passing data to VIEWS

    //method that will accept a model
    //model- represent our data that will be displayed in our views
    @GetMapping("/data/{name}")// with variable
    //                                                 model library
    public String helloName(@PathVariable String name, Model model){
        model.addAttribute("displayName", name);
        return "data-view";//data-view is name of our view which we will create.
    }



}
