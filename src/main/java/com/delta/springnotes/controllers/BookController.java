package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Book;

import com.delta.springnotes.repos.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BookController {

    private final BookRepository bookRepo;

    //var bookRepo is a type of BookRepository
    public BookController(BookRepository bookRepo) {
        this.bookRepo = bookRepo;
    }


    @GetMapping("/books")
    public String showAds(Model model){
        List<Book> adList = bookRepo.findAll();

        model.addAttribute("noBooksFound", adList.size() == 0);
        model.addAttribute("books", adList);

        return "books/index"; //name of the view

    }

    //show the create/ add a book form
    @GetMapping("/books/create")
    public String showForm(Model model){
        model.addAttribute("newBook", new Book());
        return "books/create";
    }

    @PostMapping("/books/create")
        public String createBook(@ModelAttribute Book bookToCreate){
        //save the adToCreate parameter
            bookRepo.save(bookToCreate);
            //redirect the user to the list of books -> /books
            return "redirect:/books";
        }

    //controller method to view an individual book
    @GetMapping("/books/{id}")
    public String showBook(@PathVariable long id, Model model){
        Book book =bookRepo.getOne(id); // getting an object of Book based on the id

        model.addAttribute("showBook", book);

        return "books/show"; // new  view 'show' we need to create
    }

    //controller method that will allow for our user to edit
    @GetMapping("/books/{id}/edit")
    public String showEdit(@PathVariable long id, Model model){

        Book bookToEdit = bookRepo.getOne(id);
        //giving object bookToEdit an attribute name for our views
        model.addAttribute("editBook", bookToEdit);

        return "books/edit";
    }

    @PostMapping("/books/{id}/edit")
    //your attribute names: "title", "author", "genre", "release_date", they go in our edit form
    public String updateBook(@PathVariable long id,
                             @RequestParam(name = "title") String title,
                             @RequestParam(name = "author") String author,
                             @RequestParam(name = "genre") String genre,
                             @RequestParam(name = "release_date") String release_date)
    {
        //find the book with the passed in id
        Book foundBook = bookRepo.getBookById(id);

        //update the book's title and description
        foundBook.setTitle(title);
        foundBook.setAuthor(author);
        foundBook.setGenre(genre);
        foundBook.setRelease_date(release_date);

        //save the new book's data changes
        bookRepo.save(foundBook);

        //redirect the user to the list of books
        return "redirect:/books/";
    }
    //controller method that will allow the user to delete objects of out class
    @PostMapping("/books/{id}/delete")
    public String delete(@PathVariable long id){
        //access the deleteById()method from the repository
        bookRepo.deleteById(id);

        //redirect the user to the url containing
        //list of ads
        return  "redirect:/books/";
    }



}//end of class
