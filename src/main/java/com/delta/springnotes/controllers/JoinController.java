package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class JoinController {


    //assign a specific url
    @GetMapping("/join")

    public String showForm(){
        return "join-view"; // is the name of our view
    }
    //set up a POST method for our views data

    @PostMapping("/join")
    //add method, add two parameters
    public String dispalyData(@RequestParam(name = "classroom")//classroom comes from the form name attribute
                              String classroom, Model model){
        model.addAttribute("classroom", classroom);
        return "join-view";
    }



}
