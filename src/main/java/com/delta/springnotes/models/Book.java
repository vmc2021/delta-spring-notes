package com.delta.springnotes.models;


import javax.persistence.*;

@Entity

@Table(name = "books")


public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 100)
    private String title;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private String genre;

    @Column(nullable = false)
    private String release_date;

    //constructor
    public Book(long id, String title, String author, String genre, String release_date) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.release_date = release_date;
    }

    //empty constructor
    public Book() {
    }


    //getter/setters


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
}//end of class
