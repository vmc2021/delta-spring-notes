package com.delta.springnotes.repos;

import com.delta.springnotes.models.Ad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


//interface
public interface AdRepository extends JpaRepository<Ad, Long> {

    //the following method is equivalent to the built-in 'getOne' Method
    @Query("from Ad a where a.id like ?1")//mysql statement
    Ad getAdById(long id);//statement represented by the getAdById method

}

